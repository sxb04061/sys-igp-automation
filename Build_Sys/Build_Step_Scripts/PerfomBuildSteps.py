import os
import subprocess
import logging
import pexpect


class BuildCreation:

    def __init__(self):
        """
        Initialize any local variable here
        """
        self.log = logging.getLogger(__name__)

    def build_creation(self, dir_path, type, img_tag):
        """
        Create build based on the user input
        :return: True/False
        """

        # 1. Login to the registry.prosoft.io

        self.log.info("PWD :{}\nLogin to registry.prosoft.io".format(dir_path))
        print("PWD :{}\nLogin to registry.prosoft.io".format(dir_path))

        # 2. Note - in order to avoid error while login in docker registry.prosoft.io change to super user
        """su_user = subprocess.getoutput("echo $USER")
        print("Login in as super user :", su_user)
        chg_root = pexpect.spawn("sudo su")
        chg_root.expect(".*password.*:")
        chg_root.send("root\n")"""

        pwd = os.getcwd()
        self.log.info("Current pwd :{}".format(pwd))

        # 3. Change the directory to the directory where docker has to be login and login registry.prosoft.io
        os.chdir(dir_path)
        self.log.info("Now current pwd :{}".format(os.getcwd()))

        login_output = subprocess.getoutput("docker login registry.prosoft.io")
        if "Login Succeeded" in login_output:
            self.log.info("Login to registry.prosoft.io successful :\nLogin output\n{}".format(login_output))
            print("Login to registry.prosoft.io successful")
        else:
            self.log.warning("Login to registry.prosoft.io unsuccessful :\nLogin output\n{}".format(login_output))
            print("Login to registry.prosoft.io unsuccessful")

        # 4. Change super user back to non-root user and again change directory to the repo directory
        """self.log.info("Exiting super user")
        su_pass_output = pexpect.spawn("su - {}".format(su_user))
        su_pass_output.send("root\n")
        print("Exited super user")"""

        os.chdir(dir_path)
        self.log.info("Changed directory to :{}".format(os.getcwd()))
        print("Changed directory to :{}".format(os.getcwd()))

        # 5. Pull IGP build container image from prosoft.io
        self.log.info("Pulling the IGP container build image with image tag :{}".format(img_tag))
        print("Pulling the IGP container build image with image tag :{}".format(img_tag))

        img_pull_output = subprocess.getoutput("docker pull registry.prosoft.io/prosoft/igp_build_container:{}".format(img_tag))
        print("Pull request output :\n{}\n".format(img_pull_output))
        self.log.info("Pull request output :\n{}\n".format(img_pull_output))
        if "Image is up to date" in img_pull_output:
            print("Pull request successful")
        elif "Error response from daemon:" in img_pull_output:
            print("Image :{} doesn't exists. Exiting build process".format(img_tag))
            return False

        # 6. Based on the user input i.e. - IMX8MM/QEMU create the build
        if type.lower() == "imx8mm" or type.lower() == "imx8":
            """
            This section is to be discussed later on
            """

        elif type.lower() == "qemu":
            self.log.info("Creating a build using container version :{}".format(img_tag))
            print("Creating a build using container version :{}".format(img_tag))
            qemu_img_output = subprocess.getoutput("./igp_builder.sh qemu {}".format(img_tag))
            if "the input device is not a TTY" in qemu_img_output:
                print("Qemu image build process failed. Exiting Now!!")
                return False
            else:

                # print("Qemu image pull output :\n", qemu_img_output)
                self.log.info("Qemu image pull output :\n{}\n".format(qemu_img_output))
                print("Successfully created the build for QEMU")

                # Check the successful build creation - Check Image rootfs.ext2 rootfs.ext4 files/folder
                buildfiles_status = self.check_build_files(dir_path, type)
                return buildfiles_status

    def check_build_files(self, dir_path, type):
        """
        Check whether the content in directory is correct or not
        :return: True / False
        """
        print("Checking the files after completion of build process")
        dir_path = os.path.join(dir_path, "buildroot-2020.02.x/output/images")
        self.log.info("Checking the files and folder after build process :{}".format(dir_path))
        print("Checking the files and folder after build process :{}".format(dir_path))
        qemu_expected_content = ['Image', 'rootfs.ext2', 'rootfs.ext4']
        imx8mm_expected_content = ['fsl-imx8mm-evk.dtb', 'Image', 'rootfs.ext2',
                                   'rootfs.ext4', 'sdcard.img', 'u-boot.bin',
                                   'u-boot.imx', 'u-boot-spl.bin']

        flag = 0
        folder_content = subprocess.getoutput("ls {}".format(dir_path)).split("\n")
        if type.lower() == 'qemu':
            expected_content = qemu_expected_content
        else:
            expected_content = imx8mm_expected_content

        for files in folder_content:
            if files in expected_content:
                flag = 1
                continue
            else:
                flag = -1
                break
        if flag == -1:
            self.log.critical("Files/Folder under the build directory are not same")
            print("Files/Folder under the build directory are not same")
            return False
        elif flag == 1:
            self.log.info("Files/Folder under the build directory are same")
            print("Files/Folder under the build directory are same")
            return True


build_object = BuildCreation()

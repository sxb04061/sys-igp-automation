import os
import re
import time
import subprocess
import logging
import pexpect
import datetime


class QemuDockerValidation:

    def __init__(self):
        """
        Initialize the object here
        """
        self.log = logging.getLogger(__name__)

    def docker_installation_validation(self, root_password="root"):
        """
        Check whether docker is installed or not.
        If docker is not installed then install it.
        :return:
        """

        # Check if docker is already installed or not
        print("Checking whether docker is installed or not")
        self.log.info("Checking whether docker is installed or not")

        # If docker is installed return True
        docker_version = subprocess.getoutput("docker --version")
        if "Docker version" in docker_version:
            version = re.search(r'Docker version\s+(\d+\.\d+\.\d+)', docker_version)
            print("Docker version :{}".format(version.group(1)))
            self.log.info("Docker version :{}".format(version.group(1)))

        # If docker is not installed, install docker and return True on success
        elif "Command 'docker' not found" in docker_version\
                or "No such file or directory" in docker_version\
                or "not found" in docker_version:

            print("Installing docker")
            self.log.info("Installing docker")

            # 1. fire command sudo apt install docker.io
            child_process = pexpect.spawn("sudo apt install docker.io")
            child_process.send("root\n")
            """if child_process.expect("Do you want to continue? [Y/n]"):
                child_process.send("Y")"""
            time.sleep(40)
            self.log.info("Writing docker installation output in the file")
            base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
            docker_log = os.path.join(base_dir,
                                   "Logs/docker_{}.log".format(datetime.datetime.now().strftime("%d-%m-%Y_%H:%M:%S")))

            docker_logs = open(docker_log, "w")
            for lines in child_process.readlines():
                docker_logs.write(lines.decode("utf-8"))
            docker_logs.close()
            self.log.info("Docker installation complete")
            print("Docker installation complete")
            print(subprocess.getoutput("docker --version"))


            """child_process.expect("[sudo] .*:")
            child_process.send(root_password)
            child_process.expect("Do you want to continue? [Y/n]")
            child_process.send("Y")

            self.log.info("Writing docker installation output in the file")
            docker_logs = open("docker.log", "w")
            for lines in child_process.readlines():
                docker_logs.write(lines)
            docker_logs.close()
            self.log.info("Docker installation complete")
            print("Docker installation complete")"""

    def qemu_installation_validation(self):
        """
        Check whether qemu is installed or not.
        If Qemu is not installed then install it.
        :return:
        """

        # Check if qemu is already installed or not
        print("Checking whether qemu is installed or not")
        self.log.info("Checking whether qemu is installed or not")

        # If qemu is installed return True
        qemu_version = subprocess.getoutput("qemu-system-arm --version")
        if "QEMU emulator version" in qemu_version:
            version = re.search(r'QEMU emulator version\s+(\d+\.\d+\.\d+)', qemu_version)
            print("Qemu version :{}".format(version.group(1)))
            self.log.info("Qemu version :{}".format(version.group(1)))

        # If qemu is not installed, install qemu and return True on success
        elif "Command 'qemu-system-arm' not found" in qemu_version\
                or "No such file or directory" in qemu_version\
                or "not found" in qemu_version:

            print("Installing qemu-system-arm")
            self.log.info("Installing qemu-system-arm")

            # 1. fire command sudo apt-get install qemu-system-arm
            child_process = pexpect.spawn("sudo apt-get install qemu-system-arm")
            child_process.send("root\n")
            """if child_process.expect("Do you want to continue? [Y/n]"):
                child_process.send("Y")"""
            time.sleep(40)
            self.log.info("Writing docker installation output in the file")
            base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
            qemu_log = os.path.join(base_dir,
                                      "Logs/qemu_{}.log".format(
                                          datetime.datetime.now().strftime("%d-%m-%Y_%H:%M:%S")))

            qemu_logs = open(qemu_log, "w")
            for lines in child_process.readlines():
                qemu_logs.write(lines.decode("utf-8"))
            qemu_logs.close()
            self.log.info("QEMU installation complete")
            print("QEMU installation complete")
            print(subprocess.getoutput("qemu-system-arm --version"))


val_object = QemuDockerValidation()
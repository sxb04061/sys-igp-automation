import os
import subprocess
import pexpect
import logging

class SettingQemu:

    def __init__(self):
        """
        Initialize any object here
        """
        self.log = logging.getLogger(__name__)

    def compile_qemu_simulator(self, dir_path):
        """
        Compile the qemu simulator here
        :param dir_path: Path of the directory where commands will be executed
        :return: True or false
        """



        # Make sure that you are in the right directory
        pwd = subprocess.getoutput("pwd")
        if pwd == dir_path:
            self.log.info("You are in the right directory :{}".format(dir_path))
        else:
            self.log.info("Changing directory to :{}".format(dir_path))
            os.chdir(dir_path)

        # 1. Run Qemu in host environment after generating QEMU BUILD
        print("Running QEMu in the host environment")
        self.log.info("Running QEMu in the host environment")

        run_qemu_process = pexpect.spawn("./run_qemu.sh")
        run_qemu_process.expect("IGP login:")
        run_qemu_process.send("root\n")
        run_qemu_process.expect("Password:")
        run_qemu_process.send("i\n")

        # 2. Shutdown previous QEMU sessions and make QEMU have access to internet
        print("Shutting down all the QEMU sessions and making sure QEMU has access to internet")
        self.log.info("Shutting down all the QEMU sessions and making sure QEMU has access to internet")
        pwd = subprocess.getoutput("pwd")
        print("PWD :", pwd)
        """close_qemu_sessions = pexpect.spawn("sudo ./run_qemu_netowrk.sh")
        close_qemu_sessions.expect("Login:")
        close_qemu_sessions.send("root\n")
        close_qemu_sessions.expect("Password:")
        close_qemu_sessions.send("i\n")"""

compile_object = SettingQemu()

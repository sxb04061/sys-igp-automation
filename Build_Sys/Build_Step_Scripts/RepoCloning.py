import os
import re
import time
import platform
import logging
import subprocess
import pexpect
import selenium
from selenium import webdriver
from SupportScripts.Common import common
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver import ActionChains


class CloneMethod:

    def __init__(self):
        """
        Initialize objects here
        """
        self.log = logging.getLogger(__name__)

    def https_clone_method(self):
        """
        This is HTTPS clone method
        :return:
        """
        pass

    def ssh_clone_method(self, directory, bitbucket_email, okta_email, okta_password):
        """
        This is ssh clone method. This function performs below steps -
        1. Setup your default identity
        2. Add key to the ssh agent
        :return:
        """

        try:

            # 1. check whether the directory where cloning will be done exists or not
            self.log.info("This is ssh clone method")
            print("Checking repo directory exists or not.")
            directory_status = common.check_directory_exist(directory)

            if directory_status:
                # cd into the sys-igp directory
                os.chdir(path=directory)
                changed_dir = subprocess.getoutput("pwd")
                if directory == changed_dir:
                    print("Directory changed to :{}".format(changed_dir))
                    self.log.info("Directory changed to :{}".format(changed_dir))
                else:
                    print("Couldn't change directory.... exiting setup")
                    return False

            # 2. check if the id_rsa and id_rsa.pub exists or not and ssh key is same at bitbucket settings.

            self.log.info("Calling check_private_public_files method!")
            pri_pub_file_status, ssh_command = self.check_private_public_files(bitbucket_email, okta_email, okta_password)


            if pri_pub_file_status is True:
                status, dir_path = self.execute_command(ssh_command)
                if status is True:
                    self.log.info("ssh cloning successful")
                    return True, dir_path
                else:
                    self.log.critical("ssh cloning failed")
                    return False
            else:
                self.log.info("Calling ssh_method_add_public_key")
                add_key_status, ssh_command = self.ssh_method_create_public_key(bitbucket_email, okta_email, okta_password)
                if add_key_status is True:
                    status, dir_path = self.execute_command(ssh_command)
                    if status is True:
                        self.log.info("ssh cloning successful")
                        return True, dir_path
                    else:
                        self.log.critical("ssh cloning failed")
                        return False
                elif add_key_status is not True:
                    self.log.critical("Couldn't add public key in bitbucket personal setting")
                    return False

        except Exception as error:
            print("Exception occured due to the reason :\n{}".format(error))
            self.log.info("Exception occured due to the reason :\n{}".format(error))
            return False

    def check_private_public_files(self, bitbucket_email, okta_email, okta_password):
        """
        Check if the private and public key files exists or not.
        If they exists then check the ssh key is same in bitbucket personal settings or not.
        If the ssh key is same copy the clone command and execute it in the given repo path.
        Else return False and delete the public private key files and create them new.
        :return:
        """

        print("Checking id_rsa and id_rsa.pub already exists or not")
        self.log.info("Checking id_rsa and id_rsa.pub already exists or not")

        files = subprocess.getoutput("ls ~/.ssh").split("\n")

        # if private_key_file == "id_rsa" and public_key_file == "id_rsa.pub":
        if "id_rsa" and "id_rsa.pub" in files:
            print("Public and private files exists")
            self.log.info("Public and private files exists")

            # Store the id_rsa.pub key in a variable
            public_key = subprocess.getoutput("cat ~/.ssh/id_rsa.pub")
            self.log.info("Public SSH key :\n{}".format(public_key))

            # Here - Write the code to check if ssh key in id_rsa.pub is same as in bitbucket settings.
            # if the ssh key is not same delete id_rsa and id_rsa.pub and perform function to add ssh key in settings.
            ssh_command = self.bitbucket_add_ssh_key(bitbucket_email, okta_email, okta_password, public_key)
            return True, ssh_command

        else:
            print("Public and private files not created!")
            self.log.info("Public and private files not created!")
            return False, None


    def ssh_method_create_public_key(self, bitbucket_email, okta_email, okta_password):
        """
        This method is called if the id_rsa and id_rsa.pub file has to be created again.
        This method creates them and add the ssh-key to the bitbucket personal settings.
        At the end this method returns True when the ssh-key is successfully added and
        ssh-clone command to clone sys-igp repo at the user specified directory.
        :return: True, ssh_command
        """

        # Setup your default ssh identity
        PLATFORM = platform.system()
        print("Platform :{}".format(PLATFORM))

        # 1. Enter ssh-keygen command
        print("Setting up default ssh-keygen")
        child_process = pexpect.spawn("ssh-keygen")

        # 2. Enter and keep the specific location same as default
        child_process.expect("Enter file in which to save the key .*:")
        child_process.send("\n")

        # 3. Re-enter the ssh-keygen command
        child_process.expect("Enter passphrase .*:")
        child_process.send("\n")

        child_process.expect("Enter same passphrase again:")
        child_process.send("\n")

        # 4. Print the complete output
        print("Complete ssh-keygen output:")
        for lines in child_process.readlines():
            print(lines.decode("utf-8"))

        # 4. List the content ls ~/.ssh - id_rsa, id_rsa.pub
        self.log.info("Check id_rsa and id_rsa.pub files are created or not")
        ssh_folder_content = subprocess.getoutput("ls ~/.ssh").split("\n")
        private_key_file = None
        public_key_file = None
        if "id_rsa" and "id_rsa.pub" in ssh_folder_content:
            private_key_file = "id_rsa"
            public_key_file = "id_rsa.pub"
            print("Public and private files created successfully!")
            self.log.info("Public and private files created successfully!")
        else:
            print("Public and private files not created!")
            self.log.info("Public and private files not created!")
            return False

        # Add the key to ssh-agent

        # 1. Start the agent
        print("Starting the ssh-agent")
        self.log.info("Starting the ssh-agent")
        agent_id = subprocess.getoutput("eval `ssh-agent`")
        id = re.compile('\d+')
        print("Agent id :", id.findall(agent_id))

        # 2. Enter ssh-add with the path of the private key file
        if PLATFORM.lower() == 'linux':
            pkey_addition_output = subprocess.getoutput("ssh-add ~/.ssh/{}".format(private_key_file))
            print("pkey addition output :\n", pkey_addition_output)
            if "Identity added:" in pkey_addition_output:
                print("Identity added successfully")
        elif PLATFORM.lower() == "macos":
            pkey_addition_output = subprocess.getoutput("ssh-add -K ~/.ssh/{}".format(private_key_file))
            print("pkey addition output :\n", pkey_addition_output)
            if "Identity added:" in pkey_addition_output:
                print("Identity added successfully")

            # 3. If the platform is macOS then add extra info in the /.ssh/config file
            config_file = open(file="~/.ssh/config", mode="w")
            config_file.write("Host *\n UseKeychain yes")
            config_file.close()

        # 3. Store the id_rsa.pub key in a variable
        public_key = subprocess.getoutput("cat ~/.ssh/{}".format(public_key_file))
        self.log.info("Public SSH key :\n{}".format(public_key))

        # 4. add ssh key to the bitbucket
        ssh_command = self.bitbucket_add_ssh_key(bitbucket_email, okta_email, okta_password, public_key)
        return True, ssh_command

    def bitbucket_add_ssh_key(self, bitbucket_email, okta_email, okta_password, public_key):
        """
        If the id_rsa.pub file already exists check if the public key is already added or not
        :return:
        """

        # Add the public key to your Account settings
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        gecko_executable_path = os.path.join(base_dir, "selenium-drivers/geckodriver-linux/geckodriver")
        self.log.info("Gecko driver path :\n{}".format(gecko_executable_path))

        # Create a webdriver object
        driver = webdriver.Firefox(executable_path=gecko_executable_path)

        # 1. Open the login page - "https://bitbucket.org/account/settings/ssh-keys/"
        bitbucket_url = "https://bitbucket.org/account/settings/ssh-keys/"
        driver.get(bitbucket_url)
        driver.maximize_window()
        self.log.info("Login to bitbucket url :\n{}".format(bitbucket_url))

        # 2. Enter the bitbucket_email and press continue
        bitbucket_signin = driver.find_element_by_id("username")
        bitbucket_signin.send_keys(bitbucket_email)

        bitbucket_submit = driver.find_element_by_id("login-submit")
        bitbucket_submit.click()

        # 3. Enter the okta_email and password and continue
        driver.implicitly_wait(15)

        okta_signin_username = driver.find_element_by_id("okta-signin-username")
        okta_signin_username.send_keys(okta_email)

        okta_signin_password = driver.find_element_by_id("okta-signin-password")
        okta_signin_password.send_keys(okta_password)

        okta_signin_submit = driver.find_element_by_id("okta-signin-submit")
        okta_signin_submit.click()

        # driver.implicitly_wait(25)
        print("Successfully login bitbucket profile, now adding public ssh-key")
        self.log.info("Successfully login bitbucket profile, now adding public ssh-key")

        # 4. Click on the Add key button
        time.sleep(15)
        add_button_xpath = driver.find_element_by_xpath("//button[@id='add-key' and text()='Add key']")
        ActionChains(driver).move_to_element(add_button_xpath).click().perform()

        # 5. Add Label
        time.sleep(10)
        label_text_field = driver.find_element_by_xpath(
            "//div[@class='aui-layer aui-dialog2 aui-dialog2-large'][@aria-hidden='false']//input[@id='id_label']")
        ActionChains(driver).move_to_element(label_text_field).click().send_keys("Default public key").perform()

        # 6. Add Key in text box
        time.sleep(5)
        key_text_field = driver.find_element_by_xpath(
            "//div[@class='aui-layer aui-dialog2 aui-dialog2-large'][@aria-hidden='false']//textarea[@id='id_key']")
        ActionChains(driver).move_to_element(key_text_field).click().send_keys(public_key).perform()

        # 7. Click on the Add Key button
        # add_key_button = driver.find_element_by_xpath("//div[@class='aui-layer aui-dialog2 aui-dialog2-large'][//button[@class=' aui-button aui-button-primary dialog-submit']][//button[text()='Add key']]")
        add_key_button = driver.find_element_by_xpath(
            "//div[@class='aui-layer aui-dialog2 aui-dialog2-large']//footer[@class='aui-dialog2-footer']//div[@class='aui-dialog2-footer-actions']//button[@class=' aui-button aui-button-primary dialog-submit']")
        ActionChains(driver).move_to_element(add_key_button).click().perform()

        # 8. Check if there is error while adding key. If error press cancel buttons and go to repository

        try:
            check_ssh_error = driver.find_element_by_xpath(
                "//div[@class='error' and text()='Someone has already added that SSH key.']")
            print("SSH key error :\n{}".format(check_ssh_error.text))
            self.log.info("SSH key error :\n{}".format(check_ssh_error.text))
            cancel_button = driver.find_element_by_xpath("//button[@class='aui-button aui-button-subtle dialog-cancel' and text()='Cancel']")
            cancel_button.click()
        except Exception as error:
            self.log.debug("SSH key is already present error :\n{}".format(error))
            print("SSH key is already added proceeding to repository")

        # 8. Click on the repository and go to repositories and search sys-igp
        reposirotries = driver.find_element_by_xpath("//div[@class='css-1d1qvgf' and text()='Repositories']")
        ActionChains(driver).move_to_element(reposirotries).click().perform()

        time.sleep(20)
        search_box = driver.find_element_by_xpath("//input[@class='css-13hc3dd']")
        search_box.send_keys("sys-igp")

        time.sleep(10)

        # 9. Click on the sys-igp icon
        sys_igp_icon = driver.find_element_by_xpath("//a[@title='sys-igp']//span[@aria-label='sys-igp']")
        ActionChains(driver).move_to_element(sys_igp_icon).click().perform()

        # 10. After redirecting to the sys-igp page click on clone and copy the ssh repo command
        time.sleep(10)
        clone_button = driver.find_element_by_xpath("//span[text()='Clone']")
        clone_button.click()

        # 11. switch to the box and select SSH and copy the command
        drop_down = driver.find_element_by_xpath("//span[@class='sc-bxivhb dLvSwS' and @aria-label='open']")
        drop_down.click()

        ssh_selection = driver.find_element_by_xpath("//div[@class=' css-lrg2au-singleValue' and text()='SSH']")
        ssh_selection.click()

        ssh_clone_command = driver.find_element_by_xpath(
            "//input[@class='css-13hc3dd' and @value='git clone git@bitbucket.org:belden/sys-igp.git']")
        ssh_command = ssh_clone_command.get_attribute("value")
        print("SSH clone command :{}".format(ssh_command))

        # close the box
        close_button = driver.find_element_by_xpath("//span[text()='Close']")
        close_button.click()

        # Click on the profile Global ITem
        time.sleep(5)
        global_item_button = driver.find_element_by_xpath(
            "//button[@id='profileGlobalItem' and @data-testid='GlobalNavigationItem']")
        global_item_button.click()

        logout = driver.find_element_by_xpath("//span[text()='Logout']")
        logout.click()
        print("Successfully logged out!")
        self.log.info("Successfully logged out!")
        driver.close()

        return ssh_command


    def execute_command(self, command):
        """
        Execute subprocess command on shell
        :param command: Execute command on shell
        :return: True/False
        """

        self.log.info("Executing command :{}".format(command))
        print("Executing command :{}".format(command))
        pwd = subprocess.getoutput("pwd")
        print("PWD :", pwd)
        cmd_output = subprocess.getoutput(command)
        self.log.info("ssh clone command execution status :\n{}".format(cmd_output))
        if 'Cloning into' in cmd_output:
            cloned_dir_name = re.search(r"Cloning into\s+\'(\w.+)\'", cmd_output).group(1)
            print("SSH method repo cloning is successful to dir :{}".format(cloned_dir_name))
            self.log.info("SSH method repo cloning is successful to dir :{}".format(cloned_dir_name))
            cloned_dir = os.path.join(pwd, cloned_dir_name)
            return True, cloned_dir
        elif 'fatal' in cmd_output:
            cloned_dir_name = re.search(r"\'(\w.+)\'", cmd_output).group(1)
            print("Directory already exists :{}".format(cloned_dir_name))
            self.log.info("Directory already exists :{}".format(cloned_dir_name))
            cloned_dir = os.path.join(pwd, cloned_dir_name)
            return True, cloned_dir
        else:
            print("SSH method repo cloning is unsuccessful")
            self.log.critical("SSH method repo cloning failed check error code")
            return False, None


    def check_cloned_directory_content(self, dir_name):
        """
        Check whether the content in directory is correct or not
        :return: True / False
        """
        self.log.info("Checking the files and folder of the cloned directory")
        print("Checking the files and folder of the cloned directory")
        expected_content = ['build', 'buildroot-2020.02.x', 'configuration', 'Docs',
                            'igp_builder.sh', 'IGP-source', 'README',
                            'run_qemu_network.sh', 'run_qemu.sh']
        flag = 0
        folder_content = subprocess.getoutput("ls {}".format(dir_name)).split("\n")
        self.log.info("Files/Folders under the cloned repo :\n{}".format(folder_content))
        for files in expected_content:
            if files in folder_content:
                flag = 1
                continue
            else:
                flag = -1
                break
        if flag == -1:
            self.log.critical("Files/Folder under the cloned directory are not same")
            print("Files/Folder under the cloned directory are not same")
            return False
        elif flag == 1:
            self.log.info("Files/Folder under the cloned directory are same")
            print("Files/Folder under the cloned directory are same")
            return True


# Create the object of the class
clone_object = CloneMethod()
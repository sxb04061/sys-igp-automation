import os
import re
import time
import platform
import logging
import subprocess
import pexpect
import selenium
from selenium import webdriver
from SupportScripts.Common import common
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver import ActionChains


class CloneMethod:

    def __init__(self):
        """
        Initialize objects here
        """
        self.log = logging.getLogger(__name__)

    def https_clone_method(self):
        """
        This is HTTPS clone method
        :return:
        """
        pass

    def ssh_clone_method(self, directory):
        """
        This is ssh clone method. This function performs below steps -
        1. Setup your default identity
        2. Add key to the ssh agent
        :return:
        """

        try:

            # 1. check whether the directory where cloning will be done exists or not
            self.log.info("This is ssh clone method")
            print("Checking repo directory exists or not.")
            directory_status = common.check_directory_exist(directory)

            if directory_status:
                # cd into the sys-igp directory
                os.chdir(path=directory)
                changed_dir = subprocess.getoutput("pwd")
                if directory == changed_dir:
                    print("Directory changed to :{}".format(changed_dir))
                    self.log.info("Directory changed to :{}".format(changed_dir))
                else:
                    print("Couldn't change directory.... exiting setup")
                    return False

            ssh_command = "git clone git@bitbucket.org:belden/sys-igp.git"
            status, dir_path = self.execute_command(ssh_command)
            if status is True:
                self.log.info("ssh cloning successful")
                return True, dir_path
            else:
                self.log.critical("ssh cloning failed")
                return False, None

        except Exception as error:
            print("Exception occured due to the reason :\n{}".format(error))
            self.log.info("Exception occured due to the reason :\n{}".format(error))
            return False


    def execute_command(self, command):
        """
        Execute subprocess command on shell
        :param command: Execute command on shell
        :return: True/False
        """

        self.log.info("Executing command :{}".format(command))
        print("Executing command :{}".format(command))
        pwd = subprocess.getoutput("pwd")
        print("PWD :", pwd)
        cmd_output = subprocess.getoutput(command)
        self.log.info("ssh clone command execution status :\n{}".format(cmd_output))
        if 'Cloning into' in cmd_output:
            cloned_dir_name = re.search(r"Cloning into\s+\'(\w.+)\'", cmd_output).group(1)
            print("SSH method repo cloning is successful to dir :{}".format(cloned_dir_name))
            self.log.info("SSH method repo cloning is successful to dir :{}".format(cloned_dir_name))
            cloned_dir = os.path.join(pwd, cloned_dir_name)
            return True, cloned_dir
        elif 'fatal' in cmd_output:
            cloned_dir_name = re.search(r"\'(\w.+)\'", cmd_output).group(1)
            print("Directory already exists :{}".format(cloned_dir_name))
            self.log.info("Directory already exists :{}".format(cloned_dir_name))
            cloned_dir = os.path.join(pwd, cloned_dir_name)
            return True, cloned_dir
        else:
            print("SSH method repo cloning is unsuccessful")
            self.log.critical("SSH method repo cloning failed check error code")
            return False, None

    def check_cloned_directory_content(self, dir_name):
        """
        Check whether the content in directory is correct or not
        :return: True / False
        """
        self.log.info("Checking the files and folder of the cloned directory")
        print("Checking the files and folder of the cloned directory")
        expected_content = ['build', 'buildroot-2020.02.x', 'configuration', 'Docs',
                            'igp_builder.sh', 'IGP-source', 'README',
                            'run_qemu_network.sh', 'run_qemu.sh']
        flag = 0
        folder_content = subprocess.getoutput("ls {}".format(dir_name)).split("\n")
        self.log.info("Files/Folders under the cloned repo :\n{}".format(folder_content))
        for files in expected_content:
            if files in folder_content:
                flag = 1
                continue
            else:
                flag = -1
                break
        if flag == -1:
            self.log.critical("Files/Folder under the cloned directory are not same")
            print("Files/Folder under the cloned directory are not same")
            return False
        elif flag == 1:
            self.log.info("Files/Folder under the cloned directory are same")
            print("Files/Folder under the cloned directory are same")
            return True


# Create the object of the class
clone_without_ssh_object = CloneMethod()
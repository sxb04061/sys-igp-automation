import re
import os
import time
import logging
import subprocess
import argparse
import datetime
from Build_Step_Scripts.RepoCloning import clone_object
from Build_Step_Scripts.RepoClonningWithoutSshKeyAddition import clone_without_ssh_object
from SupportScripts.Logging import logmethod
from SupportScripts.Common import common
from Build_Step_Scripts.RequisiteValidation import val_object
from Build_Step_Scripts.PerfomBuildSteps import build_object
from Build_Step_Scripts.CompilingQemuBuild import compile_object

class BuildSysMainClass:

    def __init__(self, args, log_object):
        """
        Initialize any arguments passed by the user
        """
        # create a local instance of log_object
        self.log = log_object

        # create local class instance for args
        self.img = args.img
        self.type = args.type
        self.method = args.method
        self.directory = args.directory


        """self.bitbucket_email = args.b_uname
        self.okta_email = args.okta_email
        self.okta_password = args.okta_pass"""
        # print(self.img, self.type, self.method, self.username, self.password, self.directory)


    def check_pre_requisite(self):
        """
        Check whether below are installed or not
        1. Docker
        2. Qemu
        If both are installed return status as true else download the required installations.
        When installation is successfully done return true else raise an error.
        Store the installation logs in the text file.
        :return:
        """
        print("STEP - Checking pre-requisite!\n")
        self.log.info("STEP - Checking the pre-requisite\n")

        # call the function to check whether docker is installed or not
        val_object.docker_installation_validation()

        # call the function to check whether qemu is installed or not
        val_object.qemu_installation_validation()

        # check the status of the installation and return the status.
        print("Pre-requisite satisfied!\n")
        return True

    def repo_clone_method(self):
        """
        As per user input choose the method HTTPS or SSH
        1. HTTPS - TBD
        2. SSH - Create SSH-KEY
        :return: True/False
        """
        print("\nSTEP - Cloning the repository based on the user input :{}\n".format(self.method))
        self.log.info("\nSTEP - Cloning the repository based on the user input :{}\n".format(self.method))
        repo_method_status = False
        if self.method.lower() == "https" or self.method.lower() == "http":
            pass

        elif self.method.lower() == "ssh":

            # 1. Clone the repository and return the directory path where cloning is done
            """cloning_status, dir_path = clone_object.ssh_clone_method(self.directory, self.bitbucket_email,
                                                           self.okta_email, self.okta_password)"""

            cloning_status, dir_path = clone_without_ssh_object.ssh_clone_method(self.directory)

            # 2. Check whether the cloned directory contains all the files and folder
            if cloning_status:
                repo_method_status = clone_without_ssh_object.check_cloned_directory_content(dir_path)

            return repo_method_status, dir_path

    def perform_build_steps(self, dir_path):
        """
        Perform build steps based on the user input - qemu/imx8mm
        On successful build generation return True else false
        :return: True/False
        """

        self.log.info("\nSTEP - Performing build steps based for :{}\n".format(self.type).upper())
        print("\nSTEP - Performing build steps based for :{}\n".format(self.type).upper())

        build_status = build_object.build_creation(dir_path, self.type, self.img)

        if build_status:
            self.log.info("Build creation is successful for QEMU simulator")
            print("Build creation is successful for QEMU simulator")

            """
            self.log.info("Build creation is successful perform compiling for QEMU simulator")
            print("Build creation is successful perform compiling for QEMU simulator")
            compile_object.compile_qemu_simulator(dir_path)"""

            return True
        else:

            self.log.info("Build creation is unsuccessful for QEMU simulator")
            print("Build creation is unsuccessful for QEMU simulator")

            """self.log.critical("Build creation is unsuccessful quiting compiling for QEMU simulator")
            print("Build creation is unsuccessful perform quiting for QEMU simulator")"""

            return False


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--img", help="Enter the image/build version")
    parser.add_argument("-t", "--type", help="Enter the build type")
    parser.add_argument("-m", "--method", help="Enter the clone type")
    parser.add_argument("-d", "--directory", help="Enter the path where you want to clone the directory")

    """parser.add_argument("-b_uname", "--b_uname", help="Enter the prosoft-tech email for bitbucket single login")
    parser.add_argument("-o_uname", "--okta_email", help="Enter psft.local email for okta login ")
    parser.add_argument("-o_pass", "--okta_pass", help="Enter the password for okta login")"""


    start_time = datetime.datetime.now()
    print("--------------Start Image Building Process-----------------")

    print("Time start:", start_time)

    # Create an log object
    log_obj = logmethod("build_image_{}".format(datetime.datetime.now().strftime("%d-%m-%Y_%H:%M:%S")))

    args = parser.parse_args()
    print("Arguments entered by user :\n", args, "\n")
    log_obj.info("Arguments entered by user :\n{}".format(args))

    try:

        # Create an object of the BuildSysMainClass
        build = BuildSysMainClass(args, log_obj)

        # Check - The space of the system whether it has enough space or not
        check_mem_status = common.check_memory_space()

        # Check - whether docker and qemu is installed or not
        # pre_req_status = build.check_pre_requisite()

        # Check - clone the repository based on the user input
        repo_method_status, dir_path = build.repo_clone_method()

        # Check - build steps
        if repo_method_status:
            build_step_status = build.perform_build_steps(dir_path)
        else:
            print("Exiting Build process")

    except Exception as error:
        print("Exception found :\n", error)
        log_obj.info("Exception found :\n{}".format(error))

    end_time = datetime.datetime.now()
    print("--------------Ended Image Building Process-----------------")
    print("End start:", end_time)
    print("Time take :", end_time - start_time)
import re
import os
import logging
import subprocess


class CommonFunctions:

    def __init__(self):
        """
        Initialize objects here
        """

        self.log = logging.getLogger(__name__)


    def check_directory_exist(self, dir):
        """
        Check whether the directory exists or not. If not then create the directory
        :param dir:
        :return: True/False
        """

        # check the directory
        if os.path.isdir(dir):
            self.log.info("Directory {} exists".format(dir))
            print("Directory {} exists".format(dir))
            return True
        else:
            self.log.info("Creating directory :\n{}".format(dir))
            os.makedirs(dir)
            if os.path.isdir(dir):
                self.log.info("Directory {} successfully created".format(dir))
                print("Directory {} created".format(dir))
                return True
            else:
                self.log.debug("Directory {} couldn't be created".format(dir))
                return False


    def check_memory_space(self):
        """
        Check the memory space of the system. If the system space is more than 3GB then proceed with the
        next step of image building
        :return: True/False
        """

        # Check the memory space of the system

        print('Checking the memory space of the system')
        mem_reading = subprocess.getoutput('df -h')

        print("Memory reading :\n", mem_reading, "\n")
        self.log.info("Memory reading :\n{}".format(mem_reading))

        mem_values = re.search(r'(/dev/sda5\s+(\d+\w+|\d+\.\w+)\s+(\d+\w+|\d+\.\w+)\s+(\d+\w+|\d+\.\w+))', mem_reading)
        total, used, available = mem_values.group(2), mem_values.group(3), mem_values.group(4)

        self.log.info("Space available \nTotal :{}\nUsed :{}\nAvailable :{}\n".format(total, used, available))
        print("Space available \nTotal :{}\nUsed :{}\nAvailable :{}".format(total, used, available))

        if available[-1].lower() == "g":
            if int(available[:-1]) >= 3:
                print("{} Space is available for building image\n".format(available))
                self.log.info("{} Space is available for building image\n".format(available))
                return True
        else:
            print("Space is not available please release some memory\n")
            self.log.debug("Space is not available please release some memory\n")
            return False


# created object of the class here
common = CommonFunctions()

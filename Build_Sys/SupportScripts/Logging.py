import os
import logging
import logging.config


def logmethod(filename):
    logdir = os.path.join(os.getcwd(), "Logs")

    logging.basicConfig(filename='/{}/{}.log'.format(logdir, filename),
                    filemode='w',
                    level=logging.DEBUG,
                    datefmt='%d-%m-%Y %H:%M:%S',
                    format='%(asctime)s - %(levelname)s - %(message)s')

    log_obj = logging.getLogger()
    log_obj.info("Logger object successfully created!!!!")
    return log_obj